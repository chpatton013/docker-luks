#!/usr/bin/env bash

set -euo pipefail

function usage() {
   local script="$(basename "${BASH_SOURCE[0]}")"
   echo "$script: <device> [CRYPTSETUP OPTIONS]"
   echo
   echo "Arguments:"
   echo "  device: Absolute path to unencrypted device"
   echo
   echo "Options:"
   echo "  -h, --help: display this message and exit"
}

if [ "$#" -lt 1 ]; then
   usage >&2
   exit 1
fi

if echo $@ | grep --silent --extended-regexp '^(-h|--help)$'; then
   usage
   exit 0
fi

device="$1"
shift

cryptsetup --verify-passphrase --verbose $@ luksFormat "$device"
