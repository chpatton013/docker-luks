# Docker Luks Utility

Use this container to manage Luks volumes.

## Summary

This container has several utility scripts that forward arguments onto the
appropriate `cryptsetup` command. Some of these `cryptsetup` commands require an
interactive TTY if they are not invoked with the appropriate options.

| Script    | Delegates to                   | May require TTY |
|-----------|--------------------------------|-----------------|
| `format`  | `cryptsetup luksFormat`        | Yes             |
| `open`    | `cryptsetup luksOpen`          | Yes             |
| `close`   | `cryptsetup luksClose`         | No              |
| `resize`  | `cryptsetup resize`            | No              |
| `backup`  | `cryptsetup luksHeaderBackup`  | No              |
| `restore` | `cryptsetup luksHeaderRestore` | Yes             |

Each of these scripts respond to `-h` and `--help` flags.

In order to maintain an environment in which decypted volumes can persist, you
must run this container without a command. This will cause the default command
to run, which blocks indefinitely. After the container is running, you can issue
additional commands with `docker exec`.

You can introduce persistent devices to this container by mounting volumes from
the host machine and from other containers:

* From host machine: `--volume=<host-machine-path>:<container-mount-path>`
* From other containers: `--volumes-from=<container-name>`

You should also use a volume that stores keyfiles and header backups for any
luks volumes you maintain.

This container will expose `/dev/mapper` as a docker volume, which is the
directory that luks will use to mount decrypted volumes. This will allow you to
easily forward those mounted volumes onto other containers.

## Usage

`device` and `file` refer to absolute paths within the container.

`volume` refers to the name of a luks volume.

```
# Start the container and wait for additional commands:
docker build --tag=chpatton013/docker-luks .
docker run --privileged --rm --name=luks docker-luks

# Create a volume by formatting a block device:
docker exec --interactive --tty luks format <device>

# Open a volume and mount it under `/dev/mapper`:
docker exec --interactive --tty luks open <device> <volume>

# Close a mounted volume:
docker exec luks close <volume>

# Resize an existing volume:
docker exec luks resize <volume>

# Create a header backup:
docker exec luks backup <device> <file>

# Restore from a header backup:
docker exec --interactive --tty luks restore <device> <file>
```
