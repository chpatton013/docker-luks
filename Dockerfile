FROM debian:jessie
ENV DEBIAN_FRONTEND noninteractive

# Variables ####################################################################
ENV mapper_directory "/dev/mapper"
ENV luks_open "/usr/local/bin/open"
ENV luks_close "/usr/local/bin/close"
ENV luks_resize "/usr/local/bin/resize"
ENV luks_format "/usr/local/bin/format"
ENV luks_backup "/usr/local/bin/backup"
ENV luks_restore "/usr/local/bin/restore"

# Packages #####################################################################
RUN apt-get update --quiet --quiet \
 && apt-get install --quiet --quiet --assume-yes \
    cryptsetup \
    inotify-tools \
 && rm --recursive --force /var/lib/apt/lists/*

# LUKS #########################################################################
COPY luks_open.sh "${luks_open}"
COPY luks_close.sh "${luks_close}"
COPY luks_resize.sh "${luks_resize}"
COPY luks_format.sh "${luks_format}"
COPY luks_backup.sh "${luks_backup}"
COPY luks_restore.sh "${luks_restore}"
RUN chmod +x "${luks_open}"
RUN chmod +x "${luks_close}"
RUN chmod +x "${luks_resize}"
RUN chmod +x "${luks_format}"
RUN chmod +x "${luks_backup}"
RUN chmod +x "${luks_restore}"

RUN mkdir --parents "${mapper_directory}"
VOLUME "${mapper_directory}"

# This should be the $mapper_directory variable, but we can't use the
# shell-interpreted version of CMD for signal-handling reasons.
CMD ["inotifywait", "--monitor", "/dev/mapper"]
