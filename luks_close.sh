#!/usr/bin/env bash

set -euo pipefail

function usage() {
   local script="$(basename "${BASH_SOURCE[0]}")"
   echo "$script: <volume> [CRYPTSETUP OPTIONS]"
   echo
   echo "Arguments:"
   echo "  volume: Name of luks volume"
   echo
   echo "Options:"
   echo "  -h, --help: display this message and exit"
}

if [ "$#" -lt 1 ]; then
   usage >&2
   exit 1
fi

if echo $@ | grep --silent --extended-regexp '^(-h|--help)$'; then
   usage
   exit 0
fi

device="$1"
shift

cryptsetup $@ luksClose "$device"
