#!/usr/bin/env bash

set -euo pipefail

delimiter=":"

function usage() {
   local script="$(basename "${BASH_SOURCE[0]}")"
   echo "$script: <device> <volume> [CRYPTSETUP OPTIONS]"
   echo
   echo "Arguments:"
   echo "  device: Absolute path to unencrypted device"
   echo "  volume: Name of luks volume"
   echo
   echo "Options:"
   echo "  -h, --help: display this message and exit"
}

if [ "$#" -lt 2 ]; then
   usage >&2
   exit 1
fi

if echo $@ | grep --silent --extended-regexp '^(-h|--help)$'; then
   usage
   exit 0
fi

device="$1"
volume="$2"
shift 2

cryptsetup $@ luksOpen "$device" "$volume"
