#!/usr/bin/env bash

set -euo pipefail

function usage() {
   local script="$(basename "${BASH_SOURCE[0]}")"
   echo "$script: <device> <file> [CRYPTSETUP OPTIONS]"
   echo
   echo "Arguments:"
   echo "  device: Absolute path to unencrypted device"
   echo "  file: Absolute path to header backup file"
   echo
   echo "Options:"
   echo "  -h, --help: display this message and exit"
}

if [ "$#" -lt 2 ]; then
   usage >&2
   exit 1
fi

if echo $@ | grep --silent --extended-regexp '^(-h|--help)$'; then
   usage
   exit 0
fi

device="$1"
file="$2"
shift 2

cryptsetup $@ luksHeaderRestore "$device" --header-backup-file="$file"
